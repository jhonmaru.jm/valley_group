<?php

use App\Http\Controllers\UsuarioController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Ruta para traer los datos de todos los uusarios
Route::get('users', [UsuarioController::class, 'index']);

//Ruta para crear un usuario
Route::post('users', [UsuarioController::class, 'create']);

//Ruta para tarer los datos de un usuario a editar
Route::get('users/edit/{id}', [UsuarioController::class, 'edit']);

//Ruta para enviar datos de un usuario que se desea editar
Route::put('users/update/{id}', [UsuarioController::class, 'update']);

//Ruta para eliminar un usuario
Route::delete('users/delete/{id}', [UsuarioController::class, 'delete']);