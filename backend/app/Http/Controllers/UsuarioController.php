<?php

namespace App\Http\Controllers;

use DateTime;
use Response;
use \Exception;

use App\Models\User;

use Illuminate\Http\Request;
date_default_timezone_set('America/Bogota');

class UsuarioController extends Controller
{
  //Retorna la información de todos los usuarios
  public function index(){

    $users = User::select('id', 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'cedula', 'celular', 'email')
                 ->where('estado', 1)
                 ->get();

    return Response::json(['status' => 200, 'users' => $users]);
  }

  //Función para crear un usuario
  public function create(Request $request){
    
    $fecha_hora = new DateTime();

    try {
      //Validaciones
      $data = request()->validate([
        'primer_nombre'    => 'required|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/|max:20',
        'segundo_nombre'   => 'nullable|regex:/^[ a-zA-Z]+(\s*[ a-zA-Z]*)*[ a-zA-Z]+$/|max:20',
        'primer_apellido'  => 'required|regex:/^[ a-zA-Z]+(\s*[ a-zA-Z]*)*[ a-zA-Z]+$/|max:20',
        'segundo_apellido' => 'nullable|regex:/^[ a-zA-Z]+(\s*[ a-zA-Z]*)*[ a-zA-Z]+$/|max:20',
        'celular'          => 'required|numeric',
        'cedula'           => "required|numeric|max:999999999999|min:999999|unique:users,cedula,{$request->id}",
        'email'            => "required|regex:/\S+@\S+\.\S+/|unique:users,email,{$request->id}|max:50"
      ],[
        'required'               => 'Este campo es obligatorio.',
        'numeric'                => 'Solo se permiten numeros.',
        'primer_nombre.regex'    => 'Formato invalido, solo se permiten letras.',
        'segundo_nombre.max'     => 'Solo se permite un maximo de 20 caracteres.',
        'segundo_nombre.regex'   => 'Formato invalido, solo se permiten letras.',
        'segundo_nombre.max'     => 'Solo se permite un maximo de 20 caracteres.',
        'primer_apellido.regex'  => 'Formato invalido, solo se permiten letras.',
        'segundo_apellido.max'   => 'Solo se permite un maximo de 20 caracteres.',
        'segundo_apellido.regex' => 'Formato invalido, solo se permiten letras.',
        'segundo_apellido.max'   => 'Solo se permite un maximo de 20 caracteres.',
        'cedula.max'             => 'Solo se permite un maximo de 12 caracteres.', 
        'cedula.min'             => 'Este campo debe tener almenos 6 caracteres.',
        'cedula.unique'          => 'Este numero de documento ya exite.',  
        'email.regex'           => 'Formato de correo invalido, por favor verifique.',
        'email.unique'          => 'Este correo ya existe.' 
      ]);

      //Guardar datos en la BD.
      $user = new User;
      
      $user->primer_nombre    = $request->primer_nombre;
      $user->segundo_nombre   = $request->segundo_nombre;
      $user->primer_apellido  = $request->primer_apellido;
      $user->segundo_apellido = $request->segundo_apellido;
      $user->cedula           = $request->cedula;
      $user->celular          = $request->celular;
      $user->email            = $request->email; 
      $user->estado           = 1;
      $user->created_at       = $fecha_hora;
      $user->updated_at       = $fecha_hora;

      //Retorna respuesta 200 si los datos se guardan con exito.
      if($user->save() == true){
        return Response::json([
                                'status' => 200, 
                                'nombres' => $user->primer_nombre.' '.$user->segundo_nombre.' '.$user->primer_apellido.' '.$user->segundo_apellido
                             ]);
      }
    }catch (Throwable $e){
      //Retorna mensajes de validaciones.
    
      return Response::json([
                              'status' => 422, 
                              'message' => $this->handleException($e)
                           ]);
    }catch (ValidationException $e) { 
      //Retorna mensajes de error.
      $this->assertSame($exception, $e); 
    }
  }

  //Retirna los datos del usuario a editar
  public function edit($id){

    //Selecciona el usuario a editar por medio del ID
    $user = user::select('id', 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'cedula', 'celular', 'email')
                ->where('id', $id)
                ->get();

    //Retorna el usuario cuando lo haya encontrado
    return Response::json([
      'status' => 200, 
      'user' => $user
    ]);
  }

  //Recibe los datos del usuario que se desea actualizar
  public function update(Request $request, $id){
    
    $fecha_hora = new DateTime();

    //Validaciones
    try {
      $data = request()->validate([
        'primer_nombre'    => 'required|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/|max:20',
        'segundo_nombre'   => 'nullable|regex:/^[ a-zA-Z]+(\s*[ a-zA-Z]*)*[ a-zA-Z]+$/|max:20',
        'primer_apellido'  => 'required|regex:/^[ a-zA-Z]+(\s*[ a-zA-Z]*)*[ a-zA-Z]+$/|max:20',
        'segundo_apellido' => 'nullable|regex:/^[ a-zA-Z]+(\s*[ a-zA-Z]*)*[ a-zA-Z]+$/|max:20',
        'celular'          => 'required|numeric',
        'cedula'           => "required|numeric|max:999999999999|min:999999|unique:users,cedula,{$request->id}",
        'email'            => "required|regex:/\S+@\S+\.\S+/|unique:users,email,{$request->id}|max:50"
      ],[
        'required'               => 'Este campo es obligatorio.',
        'numeric'                => 'Solo se permiten numeros.',
        'primer_nombre.regex'    => 'Formato invalido, solo se permiten letras.',
        'segundo_nombre.max'     => 'Solo se permite un maximo de 20 caracteres.',
        'segundo_nombre.regex'   => 'Formato invalido, solo se permiten letras.',
        'segundo_nombre.max'     => 'Solo se permite un maximo de 20 caracteres.',
        'primer_apellido.regex'  => 'Formato invalido, solo se permiten letras.',
        'segundo_apellido.max'   => 'Solo se permite un maximo de 20 caracteres.',
        'segundo_apellido.regex' => 'Formato invalido, solo se permiten letras.',
        'segundo_apellido.max'   => 'Solo se permite un maximo de 20 caracteres.',
        'cedula.max'             => 'Solo se permite un maximo de 12 caracteres.', 
        'cedula.min'             => 'Este campo debe tener almenos 6 caracteres.',
        'cedula.unique'          => 'Este numero de documento ya exite.',  
        'email.regex'           => 'Formato de correo invalido, por favor verifique.',
        'email.unique'          => 'Este correo ya existe.' 
      ]);

      //Actualización de datos
      $usuario = User::where('id', $id)
                     ->update([
                                'primer_nombre'    => $request->primer_nombre,
                                'segundo_nombre'   => $request->segundo_nombre,
                                'primer_apellido'  => $request->primer_apellido,
                                'segundo_apellido' => $request->segundo_apellido,
                                'cedula'           => $request->cedula,
                                'celular'          => $request->celular,
                                'email'            => $request->email,
                                'updated_at'       => $fecha_hora,
                              ]);

      //Retorna mensaje 200 si el usuario se edito de manera correcta
      if($usuario == true){
        return Response::json([
          'status'  => 200, 
          'message' => 'El usuario se edito correctamente.'
        ]);
      }
    }catch (\Exception $e){
      //Retorna mensaje de validaciones
      return Response::json([
                              'status' => 422, 
                              'message' => $e->getMessage()
                           ]);
    }catch (ValidationException $e) { 
      //Retorna mensaje de error
      $this->assertSame($exception, $e); 
    }
  }

  public function delete($id){

    //Selecciona el usuario a editar por medio del ID
    $user = user::where('id', $id)
                ->update(['estado' => 0]);

    //Retorna el usuario cuando lo haya encontrado
    return Response::json([
      'status' => 200, 
      'user' => $user
    ]);
  }

  
}
